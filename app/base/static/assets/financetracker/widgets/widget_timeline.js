widget_timeline = {

    add: function(grid, x_val, y_val, width_val, height_val) {
        grid.addWidget(`
        <div class="grid-stack-item" data-gs-x="0" data-gs-y="4" data-gs-width="6" data-gs-height="4" data-gs-min-width="4" data-gs-min-height="3">
        <div class="grid-stack-item-content" style="overflow: hidden;">
          <div class="card card-chart" style="height: 100%;">
            <div class="card-body" style="width: 100%; height: 100%;">
              <div class="chart-area" style="width: 100%; height: 100%; overflow: auto;">
                <div id="myTimeline" style="overflow: auto; margin: auto; position: relative;">
                  <div data-vtdate="">
                    Loading ...
                  </div>
                  <div data-vtdate="">
                     Loading ...
                  </div>
                  <div data-vtdate="">
                    Loading ...
                 </div>
                 <div data-vtdate="">
                  Loading ...
                 </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>`,
        // Options
        {
            x:x_val,
            y:y_val,
            width:width_val,
            height:height_val,
            id: 'WidgetTimeline'
        } 
        );
        this.init();
        },

    init: function() {
      $('#myTimeline').verticalTimeline({
        startLeft: false,
        alternate: true,
        animate: "fade",
        arrows: false
        });
    },

    update: function(api_server_uri) {
        $.getJSON(api_server_uri + "/financetracker/dashboard/_get_transactions",
        function(response) {
            if (response.status == "Success") 
            {
                var ids = response.ids
                var labels = response.labels
                var balance_data = response.balance_data
                var names = response.names
                var values = response.values
                var category_icons = response.category_icons
                var category_names = response.category_names
                var comments = response.comments
              
                // TIMELINE
                $('#myTimeline').html('');
    
                // TABLE
                // Transform and label data
                //                0   1      2            3     4      5              6              7
                table_data = zip([ids,labels,balance_data,names,values,category_icons,category_names,comments])
                named_table_data = []
                global_transaction_dict = {}
                for (var row in table_data.reverse()) {
                  // Save in global dict
                  global_transaction_dict[table_data[row][0]] = {}
                  global_transaction_dict[table_data[row][0]]['id'] = table_data[row][0]
                  global_transaction_dict[table_data[row][0]]['name'] = table_data[row][3]
                  global_transaction_dict[table_data[row][0]]['date'] = table_data[row][1]
                  global_transaction_dict[table_data[row][0]]['value'] = table_data[row][4]
                  global_transaction_dict[table_data[row][0]]['category_name'] = table_data[row][6]
                  global_transaction_dict[table_data[row][0]]['comment'] = table_data[row][7]
    
                  named_table_data.push({id:table_data[row][0], date:table_data[row][1] , balance:table_data[row][2] , name:table_data[row][3] , value:table_data[row][4]})
                
    
                  var timeline_entry = "<div data-vtdate=\""
                  if (table_data[row][4] >= 0) {
                        // Green
                      timeline_entry += "+"
                      }
    
                  var transaction_toolbar_id = 'transaction-' + table_data[row][0]
                  
                  timeline_entry += table_data[row][4] + " €" + "\">" 
                  + "<div> <div data-vticon=\"true\"> <i class=\"fa fa-" + table_data[row][5]  + " fa-2x fa-fw\" style=\"margin-top: 7.5px;\"></i> </div>"
                  if (table_data[row][4] >= 0) {
                    // Green
                  timeline_entry += "<div data-color=\"true\">rgba(0, 168, 107, 1)</div>"
                  }
                  else {
                    // Red
                  timeline_entry += "<div data-color=\"true\">rgba(205, 92, 92, 1)</div>"
                  }
                  timeline_entry += "<b>Name:</b> " + table_data[row][3] + "<br/>" + "<b>Balance:</b> " + table_data[row][2] + "<br/>" + "<b>Category:</b> " + table_data[row][6]
                  + "<br/> BLABLALBLALAB" + "<br/> 12312312312312412512" + "<br/> Das ist eine Beschreibung"
                  + "</div>"
                  + "<button id=\"" + transaction_toolbar_id + "\" class=\"btn transaction-toolbar-button \" data-toolbar=\"user-options\"><a href=\"#\" style=\"color: white;\"><i class=\"fa fa-cog\"></i></a></button>"
                      
                  // Add entry to actual timeline
                  $("#myTimeline").append(timeline_entry)
                }
    
                $('button[data-toolbar="user-options"]').toolbar({
                  content: '#toolbar-options',
                  position: 'bottom',
                  style: 'primary',
                  animation: 'flip',
                  event: 'click',
                  hideOnClick: true
                  });
    
                $('button[data-toolbar="user-options"]').on('toolbarItemClick',
                  function( event, item ) {
                      // this: the element the toolbar is attached to
                      if (item.id == "option-transaction-remove"){
                        
                        $("#dialog-remove-transaction").dialog({
                          autoOpen: false,
                          resizable: true,
                          height: "auto",
                          width: "auto",
                          modal: true,
                          buttons: {
                            "Delete item": function() {
                              // Climbe up until timeline entry
                              anchor = $('#dialog-remove-transaction').data('clickedElement');
                              node = $('#' + anchor).parent().parent().parent().remove();
                              
                              var transaction = anchor.split("-");
                              var transaction_id = transaction[1];
                              $.post( window.location.pathname, { remove_transaction_marker: "true", transaction_id: transaction_id } );
                              balance.showNotification('top','right', 'Deleting transaction...', 'trash', 1500);
                              setTimeout(function() {
                                  update_values();}, 3000);
                              
                              $( this ).dialog( "close" );
                            },
                            Cancel: function() {
                              $( this ).dialog( "close" );
                            }
                          }
                        });
                        $('#dialog-remove-transaction').data('clickedElement', this.id)
                        $("#dialog-remove-transaction").dialog("open");
                      }
                      if (item.id == "option-transaction-edit"){
                        // Set values for Update Transaction Form
                        var transaction = this.id.split("-");
                        var transaction_id = transaction[1];
                        var transaction_item = global_transaction_dict[transaction_id]
                        $('#transaction_id_update').val(transaction_item['id'])
                        $('#transaction_title_update').val(transaction_item['name'])
                        $('#transaction_category_update').val(transaction_item['category_name'])
                        $('#transaction_value_update').val(transaction_item['value'])
                        $('#transaction_comment_update').val(transaction_item['comment'])
                        $('#transaction_date_update').val(transaction_item['date'])
                        openUpdateTransactionForm()
                      }
                  }
                );
  
                $('#myTimeline').verticalTimeline({
                  startLeft: false,
                  alternate: true,
                  arrows: false
                  });
    
                balance.showNotification('top','right', 'Update timeline finished', 'check', 1500);
            }
            else
            {
                balance.showNotification('top','right', 'Failed to fetch balance data', 'ban', 3000);
            }
            
        });
      },
}