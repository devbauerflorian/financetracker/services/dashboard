widget_account_overview = {

    add: function(grid, x_val, y_val, width_val, height_val) {
        grid.addWidget(`
        <div class="grid-stack-item">
              <div class="grid-stack-item-content" style="overflow: hidden;">
                    <div class="card card-stats" style="width: 100%; height: 100%;">
                      <div class="card-body" style="width: 100%; height: 100%;">
                        <div class="row">
                          <div class="col-7">
                            <div class="numbers">
                              <p class="card-category">Account</p>
                              <h3 id="Overview-Balance" class="card-title">Loading...</h3>
                            </div>
                          </div>
                          <div class="col-3">
                            <i class="fa fa-coins fa-3x fa-fw"></i>
                         </div>
                        </div>
                        <div class="row">
                          <div class="col-7">
                            <div class="numbers">
                              <p class="card-category">Transactions</p>
                              <h3 id="Overview-Transaction-Count" class="card-title">Loading...</h3>
                            </div>
                          </div>
                          <div class="col-3">
                            <i class="fa fa-globe fa-3x fa-fw"></i>
                         </div>
                        </div>
                        <div class="row">
                          <div class="col-7">
                            <div class="numbers">
                              <p class="card-category">Last update</p>
                              <h3 id="Overview-Last-Refresh" class="card-title">Loading...</h3>
                            </div>
                          </div>
                          <div class="col-3">
                            <i class="fa fa-history fa-3x fa-fw"></i>
                         </div>
                        </div>
                      <div class="card-footer">
                        <hr>
                        <div class="stats">
                          <i class="fa fa-trophy fa-fw"></i> Account statistics
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>`  ,
            // Options
            {
              x:x_val,
              y:y_val,
              width:width_val,
              height:height_val,
              id: 'WidgetAccountOverview'
            } 
            );
        },

      update: function(api_server_uri){
          // Transaction Counter
          $.getJSON(api_server_uri + "/financetracker/dashboard/_get_transaction_count",
          function(response) {
              if (response.status == "Success") 
              {
                  var transaction_counter = response.transaction_count
      
                  $('#Overview-Transaction-Count').html(transaction_counter)
      
                  balance.showNotification('top','right', 'Update transactions finished', 'check', 1500);
              }
              else
              {
                  balance.showNotification('top','right', 'Failed to fetch balance data', 'ban', 3000);
              }
              
          });
          // Current balance
          $.getJSON(api_server_uri + "/financetracker/dashboard/_get_current_balance",
          function(response) {
                  if (response.status == "Success") 
                  {
                      $('#Overview-Balance').html(response.current_balance)
      
                      balance.showNotification('top','right', 'Update current balance finished', 'check', 1500);
                  }
                  else
                  {
                      balance.showNotification('top','right', 'Failed to fetch balance data', 'ban', 3000);
                  }
                 
              });
      
          // Last Refresh
          var currentdate = new Date(); 
          var minutes = currentdate.getMinutes();
          if(minutes < 10) {
            minutes = "0" + minutes
          }
          $('#Overview-Last-Refresh').html(currentdate.getHours() + ":" + minutes)
        },
}