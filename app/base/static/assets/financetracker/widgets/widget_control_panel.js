// Popup
function openTransactionForm() {
var options = {
    "backdrop" : true,
    "show":true
}
$('#popupFormTransaction').modal(options)
$('#transaction_date').text = "1234"
}

function openUpdateTransactionForm() {
var options = {
    "backdrop" : true,
    "show":true
}
$('#popupFormUpdateTransaction').modal(options)
}

function openCategoryForm() {
var options = {
    "backdrop" : true,
    "show":true
}
$('#popupFormCategory').modal(options)
}


function openShareForm() {
    var options = {
        "backdrop" : true,
        "show":true
    }
    $('#popupFormShares').modal(options)
}

function openSharePositionForm() {
    var options = {
        "backdrop" : true,
        "show":true
    }
    $('#popupFromSharePosition').modal(options)
}

function openCoinForm() {
    var options = {
        "backdrop" : true,
        "show":true
    }
    $('#popupFormCoins').modal(options)
}

function openCoinPositionForm() {
    var options = {
        "backdrop" : true,
        "show":true
    }
    $('#popupFromCoinPosition').modal(options)
}

    
function updateWidgets(){
    widgets.update_all_widgets()
}  

function save_grid_layout() {
    widget_helper.saveGridLayout()
}

widget_control_panel = {

    add: function(grid, x_val, y_val, width_val, height_val) {
        grid.addWidget(`
        <div class="grid-stack-item" data-gs-min-width="2" data-gs-min-height="2">
        <div class="grid-stack-item-content" style="overflow: hidden;">
            <div class="card card-stats" style="width: 100%; height: 100%;">
            <div class="card-body" style="width: 100%; height: 100%;">
                <h2>Controls</h2>
                <button class="btn" onclick="openTransactionForm()">Create new transaction</button>
                <button class="btn" onclick="openCategoryForm()">Create new category</button>
                <button class="btn" onclick="openShareForm()">Create new share type</button>
                <button class="btn" onclick="openSharePositionForm()">Open new share position</button>
                <button class="btn" onclick="openCoinForm()">Create new coin type</button>
                <button class="btn" onclick="openCoinPositionForm()">Open new coin position</button>
                <button class="btn" onclick="updateWidgets()">Refresh data</button>
                <button class="btn" onclick="save_grid_layout()">Save layout</button>
            </div>
            </div>
        </div>
        </div>`  
        ,
        // Options
        {
            x:x_val,
            y:y_val,
            width:width_val,
            height:height_val,
            id: 'WidgetControlPanel'
        } 
        );
        }
}


