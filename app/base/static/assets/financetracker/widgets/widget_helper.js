widget_helper = {
    createWidgetWithSettings: function (grid, api_server_uri, widget_id, widget_add_function) {
        $.getJSON(api_server_uri + "/financetracker/dashboard/_get_grid_settings",
        {
            // Parameters
            widget_id: widget_id
        },
        function(response) {
            if (response.status == "Success") 
            {
                // Create widget
                widget_add_function(grid, response.grid_x_pos, response.grid_y_pos, response.grid_width, response.grid_height)
                console.log("Widget: " + widget_id + "-" + response.grid_id + " : x: " + response.grid_x_pos + " y: " + response.grid_y_pos + " w: " + response.grid_width + " h: " + response.grid_height )
            }
            else
            {
                balance.showNotification('top','right', 'Failed to fetch user ui settings for. Using default values' + widget_id, 'ban', 3000);
                widget_add_function(grid, 0, 0, 5, 5)
            }
            
        });
      },

    saveGridLayout: function() {
        var items = [];

        $('.grid-stack-item.ui-draggable').each(function () {
            var $this = $(this);
            items.push({
                x: $this.attr('data-gs-x'),
                y: $this.attr('data-gs-y'),
                w: $this.attr('data-gs-width'),
                h: $this.attr('data-gs-height'),
                id: $this.attr('data-gs-id'),
                content: $('.grid-stack-item-content', $this).html()
            });
        });
        console.log(items)
        items.forEach(function(item){
            console.log("Save: " + item.id + " : x: " + item.x + " y: " + item.y + " w: " + item.w + " h: " + item.h )
            $.post( this.api_server_uri + "/financetracker/dashboard/_save_grid_layout", { grid_item_id: item.id, grid_x_pos: item.x, grid_y_pos: item.y, grid_width: item.w, grid_height: item.h} );
          });

    },
    
}