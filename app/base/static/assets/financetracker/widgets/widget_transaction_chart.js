widget_transaction_chart = {

    add: function(grid, x_val, y_val, width_val, height_val) {
        grid.addWidget(`
        <div class="grid-stack-item" data-gs-min-width="4" data-gs-min-height="3">
        <div class="grid-stack-item-content" style="overflow: hidden;">
          <div class="card card-chart" style="height: 100%;">
            <div class="card-body" style="width: 100%; height: 100%;">
              <div class="chart-area" style="width: 100%; height: 100%;">
                <canvas id="transaction_chart" style="width: 100%; height: 100%;"></canvas>
              </div>
            </div>
          </div>
        </div>
        </div>`  
        ,
        // Options
        {
            x:x_val,
            y:y_val,
            width:width_val,
            height:height_val,
            id: 'WidgetTransactionChart'
        } 
        );
        this.init();
      },
        
    init: function() {

      gradientBarChartConfiguration = {
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        title: {
              display: true,
              text: 'Transactions',
              fontSize: 15,
              fontFamily: 'sans-serif'
        },
  
        tooltips: {
          backgroundColor: '#f5f5f5',
          titleFontColor: '#333',
          bodyFontColor: '#666',
          bodySpacing: 4,
          xPadding: 12,
          mode: "nearest",
          intersect: 0,
          position: "nearest",
          callbacks: {
                      afterLabel: function(tooltipItems, data) { 
                                var tooltip = [
                                  ' ',
                                  'Date: ' + tooltipItems.xLabel
                                ]
                                return tooltip
                      },
                      title: function(tooltipItems, data) { 
                                var tooltip = [
                                  'Transaction: ' + data.my_names[tooltipItems[0].index],
                                ]
                                return tooltip
                      },
                      label: function(tooltipItems, data) { 
                          var tooltip = [
                            'Value: ' + tooltipItems.yLabel + ' €',
                          ]
                          return tooltip
                      }
                  }
        },
        responsive: true,
        scales: {
          yAxes: [{
  
            gridLines: {
              drawBorder: false,
              color: 'rgba(29,140,248,0.1)',
              zeroLineColor: "transparent",
            },
            ticks: {
              suggestedMin: 60,
              suggestedMax: 120,
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }],
  
          xAxes: [{
  
            gridLines: {
              drawBorder: false,
              color: 'rgba(29,140,248,0.1)',
              zeroLineColor: "transparent",
            },
            ticks: {
              padding: 20,
              fontColor: "#9e9e9e"
            }
          }]
        }
      };
  
      var ctx = document.getElementById("transaction_chart").getContext("2d");
  
      var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);
  
      gradientStroke.addColorStop(1, 'rgba(29,140,248,0.2)');
      gradientStroke.addColorStop(0.4, 'rgba(29,140,248,0.0)');
      gradientStroke.addColorStop(0, 'rgba(29,140,248,0)'); //blue colors
    
      widget_transaction_chart.myTransactionChart = new Chart(ctx, {
        type: 'bar',
        responsive: true,
        legend: {
          display: false
        },
        data: {
          labels: [

          ],
          datasets: [{
            label: "Countries",
            fill: true,
            backgroundColor: gradientStroke,
            hoverBackgroundColor: gradientStroke,
            borderColor: '#1f8ef1',
            borderWidth: 2,
            borderDash: [],
            borderDashOffset: 0.0,
            data: [
            ],
          }]
        },
        options: gradientBarChartConfiguration
      });
  

    },

    update: function (api_server_uri) {
      $.getJSON(api_server_uri + "/financetracker/dashboard/_get_transactions",
      function(response) {
          if (response.status == "Success") 
          {
              var ids = response.ids
              var labels = response.labels
              var balance_data = response.balance_data
              var names = response.names
              var values = response.values
              var category_icons = response.category_icons
              var category_names = response.category_names
              var comments = response.comments
  
              // TABLE
              // Transform and label data
              //                0   1      2            3     4      5              6              7
              table_data = zip([ids,labels,balance_data,names,values,category_icons,category_names,comments])
              named_table_data = []
              for (var row in table_data.reverse()) {
                named_table_data.push({id:table_data[row][0], date:table_data[row][1] , balance:table_data[row][2] , name:table_data[row][3] , value:table_data[row][4]})
              }
  
              widget_transaction_chart.myTransactionChart.data.labels.pop();
              widget_transaction_chart.myTransactionChart.data.datasets.forEach((dataset) => {
                  dataset.data.pop();
              });
  
              widget_transaction_chart.myTransactionChart.data.labels = labels;
              widget_transaction_chart.myTransactionChart.data.datasets[0].data = values;
  
              widget_transaction_chart.myTransactionChart.data.my_names = names;
              widget_transaction_chart.myTransactionChart.update();
  
              balance.showNotification('top','right', 'Update transaction chart finished', 'check', 1500);
          }
          else
          {
              balance.showNotification('top','right', 'Failed to fetch balance data', 'ban', 3000);
              //clearInterval(intervalID);
          }
          
      });
    },
  

}

             