widgets = {


    init: function(api_server_uri) {
      this.api_server_uri = api_server_uri
      widget_helper.api_server_uri = api_server_uri
      this.configured_widgets = []
    },

    create_widget: function (grid, widget_id) {
      this.configured_widgets.push(widget_id)

      if (widget_id == 'WidgetAccountOverview'){
        function add_widget(grid, grid_x_pos, grid_y_pos, grid_width, grid_height){
          // Grid - X - Y - Widht - Height
          widget_account_overview.add(grid, grid_x_pos, grid_y_pos, grid_width, grid_height);
          widget_account_overview.update(this.api_server_uri);
        }
      }
      if (widget_id == 'WidgetTimeline'){
        function add_widget(grid, grid_x_pos, grid_y_pos, grid_width, grid_height){
          // Grid - X - Y - Widht - Height
          widget_timeline.add(grid, grid_x_pos, grid_y_pos, grid_width, grid_height);
          widget_timeline.update(this.api_server_uri);
        }
      }
      if (widget_id == 'WidgetBalanceChart'){
        function add_widget(grid, grid_x_pos, grid_y_pos, grid_width, grid_height){
          // Grid - X - Y - Widht - Height
          widget_balance_chart.add(grid, grid_x_pos, grid_y_pos, grid_width, grid_height);
          widget_balance_chart.update(this.api_server_uri);
        }
      }
      if (widget_id == 'WidgetControlPanel'){
        function add_widget(grid, grid_x_pos, grid_y_pos, grid_width, grid_height){
          // Grid - X - Y - Widht - Height
          widget_control_panel.add(grid, grid_x_pos, grid_y_pos, grid_width, grid_height);
          //widget_control_panel.update(this.api_server_uri);
        }
      }
      if (widget_id == 'WidgetTabelOverview'){
        function add_widget(grid, grid_x_pos, grid_y_pos, grid_width, grid_height){
          // Grid - X - Y - Widht - Height
          widget_tabel_overview.add(grid, grid_x_pos, grid_y_pos, grid_width, grid_height);
          widget_tabel_overview.update(this.api_server_uri);
        }
      }
      if (widget_id == 'WidgetTransactionChart'){
        function add_widget(grid, grid_x_pos, grid_y_pos, grid_width, grid_height){
          // Grid - X - Y - Widht - Height
          widget_transaction_chart.add(grid, grid_x_pos, grid_y_pos, grid_width, grid_height);
          widget_transaction_chart.update(this.api_server_uri);
        }
      }
      if (widget_id == 'WidgetShareOverview'){
        function add_widget(grid, grid_x_pos, grid_y_pos, grid_width, grid_height){
          // Grid - X - Y - Widht - Height
          widget_share_overview.add(grid, grid_x_pos, grid_y_pos, grid_width, grid_height);
          widget_share_overview.update(this.api_server_uri);
        }
      }
      if (widget_id == 'WidgetCoinOverview'){
        function add_widget(grid, grid_x_pos, grid_y_pos, grid_width, grid_height){
          // Grid - X - Y - Widht - Height
          widget_coin_overview.add(grid, grid_x_pos, grid_y_pos, grid_width, grid_height);
          widget_coin_overview.update(this.api_server_uri);
        }
      }

      var add_function = add_widget

      // Request settings and create widget async
      widget_helper.createWidgetWithSettings(grid, this.api_server_uri, widget_id, add_function)
    },

    update_all_widgets:function() {
      balance.showNotification('top','right', 'Starting widget update...', 'plus', 1500);
      this.configured_widgets.forEach(widget_id => this.update_widget(widget_id));
    },

    update_widget: function (widget_id) {

      if (widget_id == 'WidgetAccountOverview'){  
          widget_account_overview.update(this.api_server_uri);
      }
      if (widget_id == 'WidgetTimeline'){
          widget_timeline.update(this.api_server_uri);
      }
      if (widget_id == 'WidgetBalanceChart'){
          widget_balance_chart.update(this.api_server_uri);
      }
      if (widget_id == 'WidgetControlPanel'){
          // No update function needed
      }
      if (widget_id == 'WidgetTabelOverview'){
          widget_tabel_overview.update(this.api_server_uri);
      }
      if (widget_id == 'WidgetTransactionChart'){
          widget_transaction_chart.update(this.api_server_uri);
      }
      if (widget_id == 'WidgetShareOverview'){
          widget_share_overview.update(this.api_server_uri);
      }
      if (widget_id == 'WidgetCoinOverview'){
          widget_coin_overview.update(this.api_server_uri);
      }
    }
};
