widget_coin_overview = {

    add: function(grid, x_val, y_val, width_val, height_val) {
        grid.addWidget(`
        <div class="grid-stack-item" data-gs-min-width="4" data-gs-min-height="3">
        <div class="grid-stack-item-content" style="overflow: hidden;">
          <div class="card card-chart" style="height: 100%;">
            <div class="card-body" style="width: 100%; height: 100%;">
              <div class="chart-area" style="width: 100%; height: 100%;">
                <canvas id="coin_overview" style="width: 100%; height: 100%;"></canvas>
              </div>
            </div>
          </div>
        </div>
        </div>`  
        ,
        // Options
        {
            x:x_val,
            y:y_val,
            width:width_val,
            height:height_val,
            id: 'WidgetCoinOverview'
        } 
        );
        this.init();
    },
        
    init: function() {

      chartOptions = {
        responsive: true,
        plugins: {
          labels: {
            render: 'value',
            fontSize: 14,
            fontStyle: 'bold',
            fontColor: '#000',
            fontFamily: '"Lucida Console", Monaco, monospace'
          },
          title: {
              display: true,
              text: 'Custom Chart Title',
              padding: {
                  top: 10,
                  bottom: 30
              },
              position: 'bottom'
          }
        },
        animation: {
            animateScale: true,
            animateRotate: true
        }
      }

      var config = {
        type: 'doughnut',
        data: {
          labels: [
          ],
          datasets: [{
            label: 'My First Dataset',
            data: [],
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(54, 162, 235)',
              'rgb(255, 205, 86)'
            ],
            hoverOffset: 4
          }],
        options: chartOptions
        },
      };
      var ctx = document.getElementById("coin_overview").getContext("2d");
      widget_coin_overview.myCoinOverview = new Chart(ctx, config);        
    },

    update: function (api_server_uri) {
      $.getJSON(api_server_uri + "/financetracker/dashboard/_get_coin_overview",
      function(response) {
          if (response.status == "Success") 
          {
              var ids = response.ids
              var amounts = response.amounts
              var tickers = response.tickers
  
              // TABLE
              // Transform and label data
              //                0   1      2            3     4      5              6              7
              table_data = zip([ids,amounts,tickers])
              named_table_data = []
              for (var row in table_data.reverse()) {
                named_table_data.push({id:table_data[row][0], amounts:table_data[row][1] , tickers:table_data[row][2]})
              }
  
              // CHARTS
              // Clear
              widget_coin_overview.myCoinOverview.data.labels.pop();
              widget_coin_overview.myCoinOverview.data.datasets.forEach((dataset) => {
                  dataset.data.pop();
              });
  
  
              widget_coin_overview.myCoinOverview.data.labels = tickers;
              widget_coin_overview.myCoinOverview.data.datasets[0].data = amounts;

              widget_coin_overview.myCoinOverview.update();
  
              balance.showNotification('top','right', 'Update coin overview finished', 'check', 1500);
          }
          else
          {
              balance.showNotification('top','right', 'Failed to fetch coin position data', 'ban', 3000);
          }
          
      });
    },
  

}

             