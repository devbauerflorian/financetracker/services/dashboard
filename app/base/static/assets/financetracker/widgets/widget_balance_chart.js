widget_balance_chart = {

    add: function(grid, x_val, y_val, width_val, height_val) {
        grid.addWidget(`
        <div class="grid-stack-item" data-gs-min-width="2" data-gs-min-height="2">
        <div class="grid-stack-item-content" style="overflow: hidden;">
          <div class="card card-chart" style="height: 100%;">
            <div class="card-body" style="width: 100%; height: 100%;">
              <div class="chart-area" style="width: 100%; height: 100%;">
                <canvas id="balance_chart"></canvas>
              </div>
            </div>
          </div>
        </div>
        </div>`  
        ,
        // Options
        {
            x:x_val,
            y:y_val,
            width:width_val,
            height:height_val,
            id: 'WidgetBalanceChart'
        } 
        );
        // Init UI element
        this.init()
        },

    init: function() {
        gradientChartOptionsConfigurationWithTooltipPurple = {
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            title: {
                display: true,
                text: 'Balance',
                fontSize: 15,
                fontFamily: 'sans-serif'
            },
        
            tooltips: {
                backgroundColor: '#f5f5f5',
                titleFontColor: '#333',
                bodyFontColor: '#666',
                bodySpacing: 4,
                xPadding: 12,
                mode: "nearest",
                intersect: 0,
                position: "nearest",
                callbacks: {
                        afterLabel: function(tooltipItems, data) { 
                                  var tooltip = [
                                    ' ',
                                    'Date: ' + tooltipItems.xLabel
                                  ]
                                  return tooltip
                        },
                        title: function(tooltipItems, data) { 
                                  var tooltip = [
                                    'Transaction: ' + data.my_names[tooltipItems[0].index],
                                    data.my_values[tooltipItems[0].index] + ' €'
                                  ]
                                  return tooltip
                        },
                        label: function(tooltipItems, data) { 
                            var tooltip = [
                              'Balance: ' + tooltipItems.yLabel + ' €',
                            ]
                            return tooltip
                        }
                    }
            },
            responsive: true,
            scales: {
                yAxes: [{
                barPercentage: 1.6,
                gridLines: {
                    drawBorder: false,
                    color: 'rgba(29,140,248,0.0)',
                    zeroLineColor: "transparent",
                },
                ticks: {
                    suggestedMin: 60,
                    suggestedMax: 125,
                    padding: 20,
                    fontColor: "#9a9a9a"
                }
                }],
        
                xAxes: [{
                barPercentage: 1.6,
                gridLines: {
                    drawBorder: false,
                    color: 'rgba(225,78,202,0.1)',
                    zeroLineColor: "transparent",
                },
                ticks: {
                    padding: 20,
                    fontColor: "#9a9a9a"
                }
                }]
            }
            };
        
        
        var ctx = document.getElementById("balance_chart").getContext('2d');
        
        var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);
        
        gradientStroke.addColorStop(1, 'rgba(72,72,176,0.1)');
        gradientStroke.addColorStop(0.4, 'rgba(72,72,176,0.0)');
        gradientStroke.addColorStop(0, 'rgba(119,52,169,0)'); //purple colors
        var config = {
        type: 'line',
        data: {
            labels: [
            ],
            datasets: [{
            label: "My First dataset",
            fill: true,
            backgroundColor: gradientStroke,
            borderColor: '#d346b1',
            borderWidth: 2,
            borderDash: [],
            borderDashOffset: 0.0,
            pointBackgroundColor: '#d346b1',
            pointBorderColor: 'rgba(255,255,255,0)',
            pointHoverBackgroundColor: '#d346b1',
            pointBorderWidth: 20,
            pointHoverRadius: 4,
            pointHoverBorderWidth: 15,
            pointRadius: 4,
            data: [
            ],
            }]
        },
        options: gradientChartOptionsConfigurationWithTooltipPurple
        };
        widget_balance_chart.myBalanceChart = new Chart(ctx, config);        
    },

    update: function (api_server_uri) {
        $.getJSON(api_server_uri + "/financetracker/dashboard/_get_transactions",
        function(response) {
            if (response.status == "Success") 
            {
                var ids = response.ids
                var labels = response.labels
                var balance_data = response.balance_data
                var names = response.names
                var values = response.values
                var category_icons = response.category_icons
                var category_names = response.category_names
                var comments = response.comments
    
                // TABLE
                // Transform and label data
                //                0   1      2            3     4      5              6              7
                table_data = zip([ids,labels,balance_data,names,values,category_icons,category_names,comments])
                named_table_data = []
                for (var row in table_data.reverse()) {
                  named_table_data.push({id:table_data[row][0], date:table_data[row][1] , balance:table_data[row][2] , name:table_data[row][3] , value:table_data[row][4]})
                }
    
                // CHARTS
                // Clear
                widget_balance_chart.myBalanceChart.data.labels.pop();
                widget_balance_chart.myBalanceChart.data.datasets.forEach((dataset) => {
                    dataset.data.pop();
                });
    
    
                widget_balance_chart.myBalanceChart.data.labels = labels;
                widget_balance_chart.myBalanceChart.data.datasets[0].data = balance_data;
                
                widget_balance_chart.myBalanceChart.data.my_names = names;
                widget_balance_chart.myBalanceChart.data.my_values = values;
    
                widget_balance_chart.myBalanceChart.update();
    
                balance.showNotification('top','right', 'Update balance chart finished', 'check', 1500);
            }
            else
            {
                balance.showNotification('top','right', 'Failed to fetch balance data', 'ban', 3000);
                //clearInterval(intervalID);
            }
            
        });
      },
}

