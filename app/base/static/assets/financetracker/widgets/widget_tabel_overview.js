widget_tabel_overview = {

    add: function(grid, x_val, y_val, width_val, height_val) {
        grid.addWidget(`
        <div class="grid-stack-item" data-gs-min-width="4" data-gs-min-height="3" data-gs-no-move="no">
        <div class="grid-stack-item-content" style="height: 100%; overflow: hidden;">
        <div class="card card-chart" style="height: 100%; overflow: hidden;"> 
            <div id="history-table" style="width: 100%; height: 100%; "></div>
            <div>
                <button class="btn" id="download-csv">Download CSV</button>
                <button class="btn" id="download-json">Download JSON</button>
                <button class="btn" id="download-xlsx">Download XLSX</button>
            </div>
        </div>
        </div>
        </div>`  
        ,
        // Options
        {
            x:x_val,
            y:y_val,
            width:width_val,
            height:height_val,
            id: 'WidgetTabelOverview'
        } 
        );
        this.init();
        },

    init: function() {

        //create Tabulator on DOM element with id "example-table"
        widget_tabel_overview.table = new Tabulator("#history-table", {
            columns:[ //Define Table Columns
            {rowHandle:true, formatter:"handle", headerSort:false, frozen:true, width:30, minWidth:30},
            {title:"Id", field:"id", formatter:"plaintext"},
            {title:"Date", field:"date", formatter:"datetime", formatterParams:{
                inputFormat:"YYYY-MM-DD",
                outputFormat:"DD.MM.YY",
                invalidPlaceholder:true,
            }, editor:widget_tabel_overview.dateEditor
            },
            {title:"Balance", field:"balance", formatter:"money", formatterParams:{
                decimal:",",
                thousand:".",
                symbol:"€",
                symbolAfter:"p",
                precision:false,
            }, editor:"number",  editorParams:{
                min:0,
                max:100,
                step:10,
                elementAttributes:{
                    maxlength:"10", //set the maximum character length of the input element to 10 characters
                },
                mask:"AAA-999",
                verticalNavigation:"table", //up and down arrow keys navigate away from cell without changing value
            }},
            {title:"Name", field:"name", formatter:"plaintext"},
            {title:"Value", field:"value", formatter:"money", formatterParams:{
                decimal:",",
                thousand:".",
                symbol:"€",
                symbolAfter:"p",
                precision:false,
            }},
            ],
            layout:"fitData",
            persistence:true, //enable table persistence
            clipboard:true, //enable clipboard functionality
            placeholder:"Loading...", //display message to user on empty table
            movableColumns: true, //enable user movable columns
            movableRows: true, //enable user movable rows
        });
    
        //trigger download of data.csv file
        document.getElementById("download-csv").addEventListener("click", function(){
            table.download("csv", "data.csv");
        });
    
        //trigger download of data.json file
        document.getElementById("download-json").addEventListener("click", function(){
            table.download("json", "data.json");
        });
    
        //trigger download of data.xlsx file
        document.getElementById("download-xlsx").addEventListener("click", function(){
            table.download("xlsx", "data.xlsx", {sheetName:"My Data"});
        });

        widget_tabel_overview.dateEditor = function(cell, onRendered, success, cancel, editorParams){
            //cell - the cell component for the editable cell
            //onRendered - function to call when the editor has been rendered
            //success - function to call to pass the successfuly updated value to Tabulator
            //cancel - function to call to abort the edit and return to a normal cell
            //editorParams - params object passed into the editorParams column definition property
        
            //create and style editor
            var editor = document.createElement("input");
        
            editor.setAttribute("type", "date");
        
            //create and style input
            editor.style.padding = "3px";
            editor.style.width = "100%";
            editor.style.boxSizing = "border-box";
        
            //Set value of editor to the current value of the cell
            editor.value = moment(cell.getValue(), "DD/MM/YYYY").format("YYYY-MM-DD")
        
            //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
            onRendered(function(){
                editor.focus();
                editor.style.css = "100%";
            });
        
            //when the value has been set, trigger the cell to update
            function successFunc(){
                success(moment(editor.value, "YYYY-MM-DD").format("DD/MM/YYYY"));
            }
        
            editor.addEventListener("change", successFunc);
            editor.addEventListener("blur", successFunc);
        
            //return the editor element
            return editor;
            };
    },

    update: function (api_server_uri) {
        $.getJSON(api_server_uri + "/financetracker/dashboard/_get_transactions",
        function(response) {
            if (response.status == "Success") 
            {
                var ids = response.ids
                var labels = response.labels
                var balance_data = response.balance_data
                var names = response.names
                var values = response.values
                var category_icons = response.category_icons
                var category_names = response.category_names
                var comments = response.comments
    
                // TABLE
                // Transform and label data
                //                0   1      2            3     4      5              6              7
                table_data = zip([ids,labels,balance_data,names,values,category_icons,category_names,comments])
                named_table_data = []
                for (var row in table_data.reverse()) {
                  named_table_data.push({id:table_data[row][0], date:table_data[row][1] , balance:table_data[row][2] , name:table_data[row][3] , value:table_data[row][4]})
                }
    
                // Update Table
                table_data = named_table_data
                widget_tabel_overview.table.replaceData(table_data);
              }
        });
    
      },
    

}



