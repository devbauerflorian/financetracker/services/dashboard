balance = {

showNotification: function(from, align, text, icon_type, time) {
    color = Math.floor((Math.random() * 4) + 1);

    var currentdate = new Date(); 
    var minutes = currentdate.getMinutes();
    if(minutes < 10) {
        minutes = "0" + minutes
    }
    var seconds = currentdate.getSeconds();
    if(seconds < 10) {
        seconds = "0" + seconds
    }
    $('#Overview-Last-Refresh').html(currentdate.getHours() + ":" + minutes)

    var notification_text = currentdate.getHours() + ":" + minutes + ":" + seconds + "     " + text
    balance.logNotification(notification_text, icon_type)

    $.notify({
      icon: "fa fa-fw fa-3x fa-" + icon_type,
      message: notification_text

    }, {
      type: type[color],
      timer: time,
      placement: {
        from: from,
        align: align
      }
    });
  },

logNotification: function(text, icon_type) {

  nav_notification_log.unshift({'text':text, 'icon':icon_type})

  var notification_content = ''
  for (entry of nav_notification_log) {
    notification_content += '<li class="nav-link"><a href="#" class="nav-item dropdown-item"> <i class="fa fa-' + entry['icon'] + ' fa-2x fa-fw" style="padding-right: 30px;"></i>' + entry['text'] + '</a></li>'
  }

  $('#navigation-notification-log').html(notification_content);

  }
}
