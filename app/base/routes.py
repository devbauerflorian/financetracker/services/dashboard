# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from flask import jsonify, render_template, redirect, request, url_for, session
from flask_login import (
    current_user,
    login_required,
    login_user,
    logout_user
)

from datetime import datetime
import functools
import requests 
from app import db, login_manager
from app.base import blueprint
from app.base.forms import LoginForm, CreateAccountForm
from app.base.models import User

from app.base.util import verify_pass
import urllib
import sys
from os.path import dirname

filedir = dirname(__file__)
sys.path.append(dirname(__file__) + '/src-gen')
print(f"Path: {sys.path}")
# API
import openapi_client
from openapi_client.rest import ApiException

import os
import json
import re
import time
# Extern - Apis

CLIENT_ID = os.environ['DASHBOARD_CLIENT_ID']
CLIENT_SECRET = os.environ['DASHBOARD_CLIENT_SECRET']
REDIRECT_URI = os.environ['DASHBOARD_REDIRECT_URI']
OAUTH_SERVER_ROOT_URI = os.environ['DASHBOARD_OAUTH_SERVER_ROOT_URI']
FINANCETRACKER_SERVER_ROOT_URI = os.environ['DASHBAORD_FINANCETRACKER_CORE_URI']
# DASHBAORD_FINNHUB_API_KEY = os.environ['DASHBAORD_FINNHUB_API_KEY']

# https://git0815
# hub.com/RomelTorres/alpha_vantage
# import finnhub
# finnhub_client = finnhub.Client(api_key=DASHBAORD_FINNHUB_API_KEY)

# WARNING ! If you change the required scopes you have to generate a new client application
REQUIRED_SCOPES = "user_information read_transactions write_transactions access_settings"

from openapi_client.models.transaction_item import TransactionItem
from openapi_client.models.transaction_detail import TransactionDetail
from openapi_client.models.category_item import CategoryItem
from openapi_client.models.value_item import ValueItem
from openapi_client.models.grid_settings_item import GridSettingsItem
from openapi_client.models.share_item import ShareItem
from openapi_client.models.share_position import SharePosition
from openapi_client.models.coin_item import CoinItem
from openapi_client.models.coin_position import CoinPosition

def api_call(func):
    """Handle api calls"""
    @functools.wraps(func)
    def wrapper_api_call(*args, **kwargs):
        configuration = openapi_client.Configuration()
        # Configure HTTP basic authorization: BasicAuth
        configuration.access_token = current_user.token
        # Defining host is optional and default to https://example.io/v1
        configuration.host = FINANCETRACKER_SERVER_ROOT_URI
        try:
            print(f"Calling {func.__name__}...")
            if not current_user.is_authenticated:
                return jsonify(status="Access denied")
            return func(configuration=configuration)
        except ApiException as e:
            print(f"Exception during API call: {func.__name__}: {e}")
            return jsonify(status="Error")

    return wrapper_api_call

@blueprint.route('/_get_share_overview')
@api_call
def _get_share_overview(configuration):
    with openapi_client.ApiClient(configuration) as api_client:
        api_instance = openapi_client.DefaultApi(api_client)
        api_response = api_instance.share_position_get(async_req=False)
        
        ids = []
        amounts = []
        tickers = []

        # store index postion of a ticker (required to calculate sums)
        ticker_index = {}

        if api_response != []: 
            for idx, share_position in enumerate(api_response):

                print(f'Share Position Item: Amount: {share_position.amount} Depot: {share_position.depot}, transaction: {share_position.open_transaction}, share: {share_position.share_type}')
                # value_data = finnhub_client.quote(share_position.share_type.ticker)
                # exchange_data = finnhub_client.forex_rates(base='USD')
                # position_value = value_data['c'] * exchange_data['quote']['EUR'] * share_position.amount
                position_value = 2000

                if share_position.share_type.ticker not in ticker_index:
                    # New share type
                    ids.append(share_position.id)
                    tickers.append(share_position.share_type.ticker)
                    ticker_index[share_position.share_type.ticker] = idx

                    amounts.append(position_value)
                else:
                    amounts[ticker_index[share_position.share_type.ticker]] += position_value

        else:
            print(f'No open share positions found.')
            return jsonify(status="Empty")

        return jsonify(status="Success",
            ids=ids,
            amounts=amounts,
            tickers=tickers,
        )

@blueprint.route('/_get_coin_overview')
@api_call
def _get_coin_overview(configuration):
    with openapi_client.ApiClient(configuration) as api_client:
        api_instance = openapi_client.DefaultApi(api_client)
        api_response = api_instance.coin_position_get(async_req=False)
        
        ids = []
        amounts = []
        tickers = []

        # store index postion of a ticker (required to calculate sums)
        ticker_index = {}

        if api_response != []: 
            for idx, coin_position in enumerate(api_response):

                print(f'Coin Position Item: Amount: {coin_position.amount} Exchange: {coin_position.exchange}, transaction: {coin_position.open_transaction}, coin: {coin_position.coin_type}')
                # value_data = finnhub_client.crypto_candles(symbol='BINANCE:' + coin_position.coin_type.ticker + 'USDT', resolution='D', _from=int(time.time() - (60*60*24)), to=int(time.time()))
                # print(f"value_data: {value_data}")
                # exchange_data = finnhub_client.forex_rates(base='USD')
                # position_value = value_data['c'][0] * exchange_data['quote']['EUR'] * coin_position.amount
                position_value = 1000
                print(f"position_value: {position_value}")
                if coin_position.coin_type.ticker not in ticker_index:
                    # New share type
                    ids.append(coin_position.id)
                    tickers.append(coin_position.coin_type.ticker)
                    ticker_index[coin_position.coin_type.ticker] = idx

                    amounts.append(position_value)
                else:
                    amounts[ticker_index[coin_position.coin_type.ticker]] += position_value

        else:
            print(f'No open coin positions found.')
            return jsonify(status="Empty")

        return jsonify(status="Success",
            ids=ids,
            amounts=amounts,
            tickers=tickers,
        )

@blueprint.route('/_get_transaction_count')
@api_call
def _get_transaction_count(configuration):
    with openapi_client.ApiClient(configuration) as api_client:
        api_instance = openapi_client.DefaultApi(api_client)
        api_response = api_instance.transactions_count_get(async_req=False)
        return jsonify(status="Success", transaction_count=api_response.transaction_count)

@blueprint.route('/_get_current_balance')
@api_call
def _get_current_balance(configuration):
    with openapi_client.ApiClient(configuration) as api_client:
        api_instance = openapi_client.DefaultApi(api_client)
        api_response = api_instance.balance_current_get(async_req=False)
        current_balance = str(api_response.balance.value) + " " + api_response.balance.currency
        return jsonify(status="Success", current_balance=current_balance)

@blueprint.route('/_get_grid_settings')
@api_call
def _get_widget_settings(configuration):
    with openapi_client.ApiClient(configuration) as api_client:
        api_instance = openapi_client.DefaultApi(api_client)
        api_response = api_instance.grid_settings_get(async_req=False, item_id=request.args.get("widget_id"))
        
        if api_response != []: 
            settings_item = api_response[0]
            print(f'Grid Item {settings_item._grid_item_id} x: {settings_item.grid_x_pos}, y: {settings_item.grid_y_pos}, w: {settings_item.grid_width}, h: {settings_item.grid_height}')

            return jsonify(status="Success",
            grid_id=settings_item._grid_item_id,
            grid_x_pos=settings_item.grid_x_pos,
            grid_y_pos=settings_item.grid_y_pos,
            grid_width=settings_item.grid_width,
            grid_height=settings_item.grid_height
            )
        else:
            print(f'Grid Item {request.args.get("widget_id")} is not created yet. Adding a default widget.')
    
@blueprint.route('/_get_transactions')
@api_call
def _get_transactions(configuration):
    with openapi_client.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = openapi_client.DefaultApi(api_client)
        api_response = api_instance.transactions_history_get(async_req=False)

        balance = 0
        labels = []
        balance_data = []
        names = []
        values = []
        ids = []
        category_names = []
        category_icons = []
        category_items = []
        comments = []

        for item in api_response:
            day = str(item.detail.timestamp).split(' ')[0]
            labels.append(day)
            balance += item.value.value
            balance_data.append(balance)
            names.append(item.name)
            values.append(item.value.value)
            ids.append(item.id)
            comments.append(item.detail.comment)
            # Category
            category_names.append(item.category.name)
            category_icons.append(item.category.icon)
        
        if len(balance_data) > 0:
            balance_now = balance_data[-1]
        else:
            balance_now = 0
        transaction_counter = len(api_response)


    return jsonify(status="Success", labels=labels, balance_data=balance_data, names=names, values=values, ids=ids, comments=comments, category_names=category_names, category_icons=category_icons, balance_now=balance_now, transaction_counter=transaction_counter)
 


@blueprint.route('/_save_grid_layout', methods=['POST'])
def grid_layout():
    if not current_user.is_authenticated:
        print("User not authentifacted")
        return redirect(url_for('base_blueprint.login'))

    print(request.method)
    print(request)
    if request.method == 'POST':
        grid_item_id = request.values.get("grid_item_id")   
        grid_x_pos = int(request.values.get("grid_x_pos"))   
        grid_y_pos = int(request.values.get("grid_y_pos"))
        grid_width = int(request.values.get("grid_width"))    
        grid_height = int(request.values.get("grid_height"))     

        print(f"x-pos: {grid_x_pos}, y-pos: {grid_y_pos}, width: {grid_width}, height: {grid_height}")    
        # Enter a context with an instance of the API client
        configuration = openapi_client.Configuration()
        # Configure HTTP basic authorization: BasicAuth
        configuration.access_token = current_user.token
        # Defining host is optional and default to https://example.io/v1
        configuration.host = FINANCETRACKER_SERVER_ROOT_URI
        with openapi_client.ApiClient(configuration) as api_client:
            # Create an instance of the API class
            api_instance = openapi_client.DefaultApi(api_client)    
            grid_item = GridSettingsItem(grid_item_id=grid_item_id, grid_x_pos=grid_x_pos, grid_y_pos=grid_y_pos, grid_width=grid_width, grid_height=grid_height)

            thread = api_instance.grid_settings_put(async_req=True, grid_settings_item=grid_item)
            api_response = thread.get()

        return ('', 204)

@blueprint.route('/balance', methods=['GET', 'POST'])
def balance():
    if not current_user.is_authenticated:
        print("User not authentifacted")
        return redirect(url_for('base_blueprint.login'))

    print(request.method)
    if request.method == 'POST':
        
        print(request.form)

        if request.form.get('share_marker') is not None:
            print('share_marker')
            name = request.form.get('share_name')
            ticker = request.form.get('share_ticker')
            isin = request.form.get('share_isin')

            if name == '' or ticker == '' or isin == '':
                # Aborted
                return ('', 204)
                        # Enter a context with an instance of the API client
            configuration = openapi_client.Configuration()
            # Configure HTTP basic authorization: BasicAuth
            configuration.access_token = current_user.token
            # Defining host is optional and default to https://example.io/v1
            configuration.host = FINANCETRACKER_SERVER_ROOT_URI
            with openapi_client.ApiClient(configuration) as api_client:
                # Create an instance of the API class
                api_instance = openapi_client.DefaultApi(api_client)
                
                item = ShareItem(ticker=ticker, name=name, isin=isin)
                
                # Send share type to server
                thread = api_instance.share_type_post(async_req=True, share_item=item)
    
            # Done
            return ('', 204)

        if request.form.get('share_marker') is not None:
            print('share_marker')
            name = request.form.get('share_name')
            ticker = request.form.get('share_ticker')
            isin = request.form.get('share_isin')

            if name == '' or ticker == '' or isin == '':
                # Aborted
                return ('', 204)
                        # Enter a context with an instance of the API client
            configuration = openapi_client.Configuration()
            # Configure HTTP basic authorization: BasicAuth
            configuration.access_token = current_user.token
            # Defining host is optional and default to https://example.io/v1
            configuration.host = FINANCETRACKER_SERVER_ROOT_URI
            with openapi_client.ApiClient(configuration) as api_client:
                # Create an instance of the API class
                api_instance = openapi_client.DefaultApi(api_client)
                
                item = ShareItem(ticker=ticker, name=name, isin=isin)
                
                # Send share type to server
                thread = api_instance.share_type_post(async_req=True, share_item=item)
    
            # Done
            return ('', 204)

        
        if request.form.get('coin_marker') is not None:
            print('coin_marker')
            name = request.form.get('coin_name')
            ticker = request.form.get('coin_ticker')

            if name == '' or ticker == '':
                # Aborted
                return ('', 204)
                        # Enter a context with an instance of the API client
            configuration = openapi_client.Configuration()
            # Configure HTTP basic authorization: BasicAuth
            configuration.access_token = current_user.token
            # Defining host is optional and default to https://example.io/v1
            configuration.host = FINANCETRACKER_SERVER_ROOT_URI
            with openapi_client.ApiClient(configuration) as api_client:
                # Create an instance of the API class
                api_instance = openapi_client.DefaultApi(api_client)
                
                item = CoinItem(ticker=ticker, name=name)
                
                # Send coin type to server
                thread = api_instance.coin_type_post(async_req=True, coin_item=item)
    
            # Done
            return ('', 204)

        if request.form.get('coin_position_marker') is not None:
            print('coin_position_marker')
            category = request.form.get('coin_position_transaction_category')
            value = request.form.get('coin_position_transaction_value')
            coin_type = request.form.get('coin_position_coin_type')
            amount = request.form.get('coin_position_amount')
            comment = request.form.get('coin_position_transaction_comment')
            date = request.form.get('coin_position_transaction_date')
            exchange = request.form.get('coin_position_exchange')
            if category == '' or value == '' or coin_type == '' or amount == '' or comment == '' or date == '' or exchange == '':
                # Aborted
                return ('', 204)
                        # Enter a context with an instance of the API client
            # Patch values for formating
            value = float(value)
            amount = float(amount)

            configuration = openapi_client.Configuration()
            # Configure HTTP basic authorization: BasicAuth
            configuration.access_token = current_user.token
            # Defining host is optional and default to https://example.io/v1
            configuration.host = FINANCETRACKER_SERVER_ROOT_URI
            with openapi_client.ApiClient(configuration) as api_client:
                # Create an instance of the API class
                api_instance = openapi_client.DefaultApi(api_client)
                
                # Request coin information
                thread = api_instance.coin_types_get(async_req=True, name=coin_type)
                coin = thread.get()

                if coin == []:
                    return ('Failed to receive coin information', 501)

                coin_item = CoinItem(ticker=coin[0].ticker, name=coin[0].name)
                
                # Create transaction
                detail_item = TransactionDetail(source="", destination="", timestamp=date, comment=comment)
                category_item = CategoryItem(icon="not set", name=category, description="not set")
                value_item = ValueItem(value=value, currency="€")
                transaction = TransactionItem(name='Open position: ' + coin_item.name, category=category_item, detail=detail_item, value=value_item)
        

                position = CoinPosition(amount=amount, coin_type=coin_item, open_transaction=transaction, close_transactions=[], comment=comment, exchange=exchange)

                # Send share type to server
                thread = api_instance.coin_position_post(async_req=True, coin_position=position)

            # Done
            return ('', 204)

        if request.form.get('share_position_marker') is not None:
            print('share_position_marker')
            category = request.form.get('share_position_transaction_category')
            value = request.form.get('share_position_transaction_value')
            share_type = request.form.get('share_position_share_type')
            amount = request.form.get('share_position_amount')
            comment = request.form.get('share_position_transaction_comment')
            date = request.form.get('share_position_transaction_date')
            depot = request.form.get('share_position_depot')
            if category == '' or value == '' or share_type == '' or amount == '' or comment == '' or date == '' or depot == '':
                # Aborted
                return ('', 204)
                        # Enter a context with an instance of the API client
            # Patch values for formating
            value = float(value)
            amount = float(amount)

            configuration = openapi_client.Configuration()
            # Configure HTTP basic authorization: BasicAuth
            configuration.access_token = current_user.token
            # Defining host is optional and default to https://example.io/v1
            configuration.host = FINANCETRACKER_SERVER_ROOT_URI
            with openapi_client.ApiClient(configuration) as api_client:
                # Create an instance of the API class
                api_instance = openapi_client.DefaultApi(api_client)
                
                # Request share information
                thread = api_instance.share_types_get(async_req=True, name=share_type)
                share = thread.get()

                print(share)
                share_item = ShareItem(ticker=share[0].ticker, name=share[0].name, isin=share[0].isin)
                
                # Create transaction
                detail_item = TransactionDetail(source="", destination="", timestamp=date, comment=comment)
                category_item = CategoryItem(icon="not set", name=category, description="not set")
                value_item = ValueItem(value=value, currency="€")
                transaction = TransactionItem(name='Open position: ' + share_item.name, category=category_item, detail=detail_item, value=value_item)
        

                position = SharePosition(amount=amount, share_type=share_item, open_transaction=transaction, close_transactions=[], comment=comment, depot=depot)

                # Send share type to server
                thread = api_instance.share_position_post(async_req=True, share_position=position)

            # Done
            return ('', 204)
    

        if request.form.get('transaction_update_marker') is not None:
            print("transaction_update_marker")
            id = request.form.get('transaction_id')
            title = request.form.get('transaction_title')
            category = request.form.get('transaction_category')
            value = request.form.get('transaction_value')
            comment = request.form.get('transaction_comment')
            date = request.form.get('transaction_date')

            print(f"title: {title}, category: {category}, value: {value}, comment: {comment}, date: {date}")            
            if title == '' or category == '' or value == '' or date == '':
                # Aborted
                return ('', 204)

            date = datetime.strptime(date, '%Y-%m-%d')
            # Patch values for formating
            value = float(value)

            # Enter a context with an instance of the API client
            configuration = openapi_client.Configuration()
            # Configure HTTP basic authorization: BasicAuth
            configuration.access_token = current_user.token
            # Defining host is optional and default to https://example.io/v1
            configuration.host = FINANCETRACKER_SERVER_ROOT_URI
            with openapi_client.ApiClient(configuration) as api_client:
                # Create an instance of the API class
                api_instance = openapi_client.DefaultApi(api_client)
                
                # Get actual transaction contents
                thread = api_instance.transaction_transaction_id_get(async_req=True, transaction_id=id)
                item = thread.get()
                print(f"Updating item with id {id}...")
                # Update conents
                if item is not None:
                    item.name = title
                    item.category.name = category
                    item.category.id = -1  # Use Name instead of id
                    item.value.value = value
                    item.detail.comment = comment
                    item.detail.timestamp = date

                    print(item)

                     # Send updated version to server
                    thread = api_instance.transaction_transaction_id_put(async_req=True, transaction_id=id, transaction_item=item)
                    api_response = thread.get()
                else:
                    return ('Failed to update user transaction', 501)
            # Done
            return ('', 204)
        

        if request.form.get('remove_transaction_marker') is not None:
            print("remove_transaction_marker")
            # Enter a context with an instance of the API client
            configuration = openapi_client.Configuration()
            # Configure HTTP basic authorization: BasicAuth
            configuration.access_token = current_user.token
            # Defining host is optional and default to https://example.io/v1
            configuration.host = FINANCETRACKER_SERVER_ROOT_URI
            with openapi_client.ApiClient(configuration) as api_client:
                # Create an instance of the API class
                api_instance = openapi_client.DefaultApi(api_client)

                transaction_id = request.form.get('transaction_id')
                print(f"Deleting transaction {transaction_id}")

                thread = api_instance.transaction_transaction_id_delete(async_req=True, transaction_id=transaction_id)
                api_response = thread.get()

            return ('', 204)
        
        if request.form.get('category_marker') is not None:
            print("Category marker")
            # Send category to server
            name = request.form.get('category_name')
            description = request.form.get('category_description')
            icon = request.form.get('category_icon')

            print(f"name: {name}, description: {description}, icon: {icon}")
            
            if name == '' or description == '' or icon == '':
                # Aborted
                return ('', 204)


            category_item = CategoryItem(icon=icon, name=name, description=description)

            # Enter a context with an instance of the API client
            configuration = openapi_client.Configuration()
            # Configure HTTP basic authorization: BasicAuth
            configuration.access_token = current_user.token
            # Defining host is optional and default to https://example.io/v1
            configuration.host = FINANCETRACKER_SERVER_ROOT_URI
            with openapi_client.ApiClient(configuration) as api_client:
                # Create an instance of the API class
                api_instance = openapi_client.DefaultApi(api_client)
                thread = api_instance.category_post(async_req=True, category_item=category_item)
                api_response = thread.get()
    
            return ('', 204)

        if request.form.get('transaction_marker') is not None:
            print("Transaction marker")
            # Send transaction to server
            title = request.form.get('transaction_title')
            category = request.form.get('transaction_category')
            value = request.form.get('transaction_value')
            comment = request.form.get('transaction_comment')
            date = request.form.get('transaction_date')

            print(f"title: {title}, category: {category}, value: {value}, comment: {comment}, date: {date}")
            if title == '' or category == '' or value == '' or date == '':
                # Aborted
                return ('', 204)

            date = datetime.strptime(date, '%d.%m.%Y')

            # Patch values for formating
            value = float(value)

            detail_item = TransactionDetail(source="", destination="", timestamp=date, comment=comment)
            category_item = CategoryItem(icon="not set", name=category, description="not set")
            value_item = ValueItem(value=value, currency="€")
            transaction = TransactionItem(name=title, category=category_item, detail=detail_item, value=value_item)
    
            # Enter a context with an instance of the API client
            configuration = openapi_client.Configuration()
            # Configure HTTP basic authorization: BasicAuth
            configuration.access_token = current_user.token
            # Defining host is optional and default to https://example.io/v1
            configuration.host = FINANCETRACKER_SERVER_ROOT_URI
            with openapi_client.ApiClient(configuration) as api_client:
                # Create an instance of the API class
                api_instance = openapi_client.DefaultApi(api_client)
                thread = api_instance.transaction_post(async_req=True, transaction_item=transaction,)
                api_response = thread.get()
    
            # Done
            return ('', 204)
    else:
        # Gen category list
        # Enter a context with an instance of the API client
        configuration = openapi_client.Configuration()
        # Configure HTTP basic authorization: BasicAuth
        configuration.access_token = current_user.token
        # Defining host is optional and default to https://example.io/v1
        configuration.host = FINANCETRACKER_SERVER_ROOT_URI
        with openapi_client.ApiClient(configuration) as api_client:
            # Create an instance of the API class
            api_instance = openapi_client.DefaultApi(api_client)

            category_thread = api_instance.categories_get(async_req=True, number=50)
            share_thread = api_instance.share_types_get(async_req=True)
            coin_thread = api_instance.coin_types_get(async_req=True)

            category_list = category_thread.get()
            category_names = []
            for item in category_list:
                category_names.append(item.name)

            share_types_list = share_thread.get()
            share_types = []
            for item in share_types_list:
                share_types.append(item.name)


            coin_types_list = coin_thread.get()
            coin_types = []
            for item in coin_types_list:
                coin_types.append(item.name)
            
        return render_template('financetracker/balance.html',
         labels=['Loading'],
         values=['data'],
         category_list=category_names,
         share_type_list=share_types,
         coin_type_list=coin_types)


@blueprint.route('/')
def route_default():
    return redirect(url_for('base_blueprint.login'))

@blueprint.route('/error-<error>')
def route_errors(error):
    return render_template('errors/{}.html'.format(error))

## Login & Registration

def save_created_state(state):
	pass
def is_valid_state(state):
	return True

import requests
import requests.auth
def get_token(code):
	client_auth = requests.auth.HTTPBasicAuth(CLIENT_ID, CLIENT_SECRET)
	post_data = {"grant_type": "authorization_code",
				 "code": code,
				 "redirect_uri": REDIRECT_URI}
	response = requests.post(OAUTH_SERVER_ROOT_URI + "oauth/token",
							 auth=client_auth,
							 data=post_data)
	token_json = response.json()
	return token_json["access_token"]

def make_authorization_url():
	# Generate a random string for the state parameter
	# Save it for use later to prevent xsrf attacks
	from uuid import uuid4
	state = str(uuid4())
	save_created_state(state)
	params = {"client_id": CLIENT_ID,
			  "response_type": "code",
			  "state": state,
			  "redirect_uri": REDIRECT_URI,
			  "duration": "temporary",
			  "scope": "identity"}
	url = OAUTH_SERVER_ROOT_URI + "oauth/authorize?" + urllib.parse.urlencode(params)
	return url

@blueprint.route('/login_callback', methods=['GET', 'POST'])
def login_callback():
    from authlib.integrations.requests_client import OAuth2Session
    client = OAuth2Session(CLIENT_ID, CLIENT_SECRET, scope=REQUIRED_SCOPES, redirect_uri=REDIRECT_URI)
    authorization_endpoint = OAUTH_SERVER_ROOT_URI + 'oauth/authorize'
    uri, state = client.create_authorization_url(authorization_endpoint)

    token_endpoint = OAUTH_SERVER_ROOT_URI + 'oauth/token'
    try:
        token = client.fetch_token(token_endpoint, authorization_response=request.url)
        print(f"Got token: {token}")
    except Exception as err:
        print(f"Failed to login: {err}")
        return redirect(url_for('home_blueprint.index'))
    
    # Authentificated
    access_token = token.get('access_token', None)
    if access_token is None:
        print(f"Error: Could not get access_token from {token}.")
        return redirect(url_for('home_blueprint.index'))
    # Request user account information
    URL = OAUTH_SERVER_ROOT_URI + "api/user_information"
    # location given here 
    location = "germany"
    # defining a params dict for the parameters to be sent to the API 
    PARAMS = {'access_token': access_token}
    print(f"Params: {PARAMS}") 
    # sending get request and saving the response as response object 
    response = requests.get(url = URL, params = PARAMS) 
    user_info = response.json()
    print(user_info)

    if (user_info.get('error','invalid request') == "success"):
        print(f"Token {token} is valid.")
    else:
        print(f"Error: Could not get user information for {token}.")
        return redirect(url_for('home_blueprint.index'))
    
    username = user_info.get('username',None)
    if username is None:
        print(f"Error: Could not get username for {token}.")
        return redirect(url_for('home_blueprint.index'))
    user = User.query.filter_by(username=username).first()
    if user is None:
        # Create user 
        
        user = User(username=user_info.get('username','Error'), first_name=user_info.get('first_name','Error'), last_name=user_info.get('username','Error'), email=user_info.get('email','Error'), token=access_token)
        print(f"Created user {user}")
        db.session.add(user)
        db.session.commit()
    else:
        # Update user token
        print("Updated user with token.")
        user.token=access_token
        db.session.commit()
    # Login
    print(f"User: {user}")
    login_user(user)
    
    return redirect(url_for('base_blueprint.balance'))

@blueprint.route('/login', methods=['GET', 'POST'])
def login():
    #return text % make_authorization_url()
     # using httpx implementation
    # using requests implementation

    login_form = LoginForm(request.form)
    if 'login' in request.form:
        # read form data

        from authlib.integrations.requests_client import OAuth2Session
        client = OAuth2Session(CLIENT_ID, CLIENT_SECRET, scope=REQUIRED_SCOPES, redirect_uri=REDIRECT_URI)
        authorization_endpoint = OAUTH_SERVER_ROOT_URI + 'oauth/authorize'
        uri, state = client.create_authorization_url(authorization_endpoint)
        return redirect (uri)

    if not current_user.is_authenticated:
        return render_template( 'login/login.html',
                                form=login_form)
    return redirect(url_for('home_blueprint.index'))


    
    #login_form = LoginForm(request.form)
    #if 'login' in request.form:
        
        # read form data
    #    username = request.form['username']
    #    password = request.form['password']

        # Locate user
    #    user = User.query.filter_by(username=username).first()
        
        # Check the password
    #    if user and verify_pass( password, user.password):

    #        login_user(user)
    #        return redirect(url_for('base_blueprint.route_default'))

        # Something (user or pass) is not ok
    #    return render_template( 'login/login.html', msg='Wrong user or password', form=login_form)

    #if not current_user.is_authenticated:
    #    return render_template( 'login/login.html',
    #                            form=login_form)
    #return redirect(url_for('home_blueprint.index'))

@blueprint.route('/create_user', methods=['GET', 'POST'])
def create_user():
    login_form = LoginForm(request.form)
    create_account_form = CreateAccountForm(request.form)
    if 'register' in request.form:

        username  = request.form['username']
        email     = request.form['email'   ]

        user = User.query.filter_by(username=username).first()
        if user:
            return render_template( 'login/register.html', msg='Username already registered', form=create_account_form)

        user = User.query.filter_by(email=email).first()
        if user:
            return render_template( 'login/register.html', msg='Email already registered', form=create_account_form)

        # else we can create the user
        user = User(**request.form)
        db.session.add(user)
        db.session.commit()

        return render_template( 'login/register.html', msg='User created please <a href="/login">login</a>', form=create_account_form)

    else:
        return render_template( 'login/register.html', form=create_account_form)

@blueprint.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('base_blueprint.login'))

@blueprint.route('/shutdown')
def shutdown():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return 'Server shutting down...'

## Errors

@login_manager.unauthorized_handler
def unauthorized_handler():
    return render_template('errors/403.html'), 403

@blueprint.errorhandler(403)
def access_forbidden(error):
    return render_template('errors/403.html'), 403

@blueprint.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404

@blueprint.errorhandler(500)
def internal_error(error):
    return render_template('errors/500.html'), 500
