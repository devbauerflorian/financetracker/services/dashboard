FROM python:3.7-alpine3.11

ENV FLASK_APP run.py

COPY run.py gunicorn-cfg.py requirements.txt config.py .env ./
COPY app app
COPY migrations migrations

ENV PATH="/root/.cargo/bin:${PATH}"
RUN set -e; \
  apk update \
  && apk add --no-cache curl \
  && curl https://sh.rustup.rs -sSf | /bin/sh -s -- -y \
  && apk add --no-cache --virtual .build-deps build-base gcc python3-dev musl-dev libffi-dev \
  && apk add --no-cache --virtual .build-deps2 libc-dev linux-headers mariadb-dev python3-dev postgresql-dev openssl-dev musl-dev \
  && pip install gevent==20.9.0 \
  && pip install -r requirements.txt \
  && pip install flask-cors flask-behind-proxy python-dateutil \
  && apk del curl .build-deps .build-deps2

COPY . /usr/src/app

EXPOSE 8021
CMD ["gunicorn", "--bind", "0.0.0.0:8021", "-w", "1", "-k", "gevent", "--config", "gunicorn-cfg.py", "run:app"]
